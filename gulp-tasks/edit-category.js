const gulp = require('gulp');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const prompt = require('prompt');
const gutil = require('gulp-util');

// Low DB
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

function editCategory() {

    let taxonomy = 'taxonomies.categories';

    // Start the prompt
    prompt.start();
    // Get two properties from the user: username and password
    prompt.get([{
        name: 'title',
        description: gutil.colors.cyan('Category to edit ?'),
        type: 'string',
        required: true
    }], function (err, result) {
        //
        // Log the results.
        //
        //gutil.log( gutil.colors.black.bgCyan.bold('\nHere are the new category informations:'));
        //gutil.log( gutil.colors.cyan('\n' + JSON.stringify(result, null, '\t')));

        const catTitle = result.title;

        let elExists = _.find(db.get(taxonomy).value(),function(t) {
            return t.title === catTitle;
        });

        if (elExists) {

            // Start the prompt
            prompt.start();
            // Get two properties from the user: username and password
            prompt.get([{
                name: 'thumbnail',
                description: gutil.colors.cyan('Nouvelle image :'),
                type: 'string',
                required: false
            }], function (err, result) {

                if (result.thumbnail !== 'undefined' && result.thumbnail !== '') {
                    db.get(taxonomy)
                        .find({ title: catTitle })
                        .assign({ thumbnail: result.thumbnail})
                        .write();
                }


                console.log('ÉLÉMENT MODIFIÉ DANS LA CATÉGORIE : ' + catTitle + ': \n' + JSON.stringify(result, null, '\t'));
            });


        } else {
            gutil.log( gutil.colors.red('Cette categorie n\'existe pas !'));
        }


    });
}

gulp.task('edit-category', function() {
    return editCategory();
});

module.exports = editCategory;