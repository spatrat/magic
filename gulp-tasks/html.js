const gulp = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const del = require('del');
const getFilepath = require('./lib/utils/getFilePath');

// Data
const siteData = require('../src/data/site-data.json');
const postsData = require('../db.json');
const globalData = Object.assign({}, siteData,postsData);


const manageEnvironment = function(environment) {
  environment.opts.autoescape = false;
};


const htmlTask = function() {

    const i = process.argv.indexOf("--file");
    if ( i === -1 ) {
        //del('./dist/*.html')
    }

    //console.log(globalData);
    return gulp.src(getFilepath(i, '.html', 'src/html'))
        .pipe(nunjucksRender({
            manageEnv: manageEnvironment,
            path: ['src/html/'],
            ext: '.html',
            data: globalData
        }))
        .pipe(gulp.dest('./dist'));
};

gulp.task('html', function() {
   htmlTask();
});


module.exports = htmlTask;