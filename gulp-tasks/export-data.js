const gulp = require('gulp');
const jsonConcat = require('gulp-json-concat');
const gutil = require('gulp-util');

const exportDataTask = function() {
    console.log('Starting javascript task!');
    return gulp.src(['./db.json', './src/data/site-data.json'])
        .pipe(jsonConcat('data.json',function(data){
            return new Buffer(JSON.stringify(data));
        }))
        .pipe(gulp.dest('dist/data'));
};


gulp.task('export-data', function() {
    exportDataTask();
    return gutil.log( gutil.colors.magenta('Export data task is done.'));
});

module.exports = exportDataTask;
