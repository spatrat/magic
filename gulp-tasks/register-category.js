const gulp = require('gulp');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const prompt = require('prompt');
const gutil = require('gulp-util');

// Low DB
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

function registerCategory() {

    let taxonomy = 'taxonomies.categories';

    // Start the prompt
    prompt.start();
    // Get two properties from the user: username and password
    prompt.get([{
        name: 'title',
        description: gutil.colors.cyan('category name ?'),
        type: 'string',
        required: true
    },{
        name: 'thumbnail',
        description: gutil.colors.cyan('category thumbnail ?'),
        type: 'string',
        required: false
    }], function (err, result) {
        //
        // Log the results.
        //
        gutil.log( gutil.colors.black.bgCyan.bold('\nHere are the new category informations:'));
        gutil.log( gutil.colors.cyan('\n' + JSON.stringify(result, null, '\t')));

        let elExists = _.find(db.get(taxonomy).value(),function(t) {
            return t.title === result.title;
        });

        if (!elExists) {
            db.get(taxonomy)
                .push(result)
                .write();
            console.log('NOUVEL ELEMENT AJOUTÉ DANS ' + taxonomy + ': \n' + JSON.stringify(result, null, '\t'));
        } else {
            gutil.log( gutil.colors.red('Cette categorie existe déjà !'));
        }


    });
}

gulp.task('register-category', function() {
   return registerCategory();
});

module.exports = registerCategory;