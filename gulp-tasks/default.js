const gulp = require('gulp');
const gulpSequence = require('gulp-sequence');
const browserSync = require('browser-sync');


gulp.task('bs', function() {

    browserSync.init({
        server: {
            baseDir: "dist",
            serveStaticOptions: {
                extensions: ['html'] // pretty urls
            }
        },
        port: 8080,
        online: true,
        notify: false
    });
});

const defaultTask = function() {
    return gulpSequence('bs', 'build', 'watch', function() {
        return console.log('Default task executed !');
    });
};

gulp.task('default', function() {
   defaultTask();
});

module.exports = defaultTask;