const gulp = require('gulp');
const browserSync = require('browser-sync');


gulp.task('watch-html', ['html'], function () {
    gulp.src('src/html/**/*.html')
        .pipe(browserSync.stream());
});

gulp.task('watch-md', ['posts','html'], function () {
    return setTimeout(function() {
            gulp.src(['src/posts/drafts/*.md', 'src/html/**/*.html', 'src/html/**/*.nunjucks' ])
                .pipe(browserSync.stream())
        }
        , 200);

});

gulp.task('watch-sass', ['sass'], function () {
    return setTimeout(function() {
        gulp.src('src/scss/**/*.scss')
            .pipe(browserSync.stream());
    }, 200);

});

gulp.task('watch-js', ['js'], function () {
    gulp.src('src/js/**/*.js')
        .pipe(browserSync.stream());
});

const watchTask = function() {
    gulp.watch(['src/html/**/*.html', 'src/html/**/*.nunjucks'], ['watch-html']);
    gulp.watch('src/posts/drafts/*.md', ['watch-md']);
    gulp.watch('src/scss/**/*.scss', ['watch-sass']);
    gulp.watch('src/js/**/*.js', ['watch-js']);
};

gulp.task('watch', function() {
   watchTask();
});
module.exports = watchTask;