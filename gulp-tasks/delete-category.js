const gulp = require('gulp');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const prompt = require('prompt');
const gutil = require('gulp-util');

// Low DB
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

function deleteCategory() {

    let taxonomy = 'taxonomies.categories';

    // Start the prompt
    prompt.start();
    // Get two properties from the user: username and password
    prompt.get([{
        name: 'title',
        description: gutil.colors.red('Category to delete ?'),
        type: 'string',
        required: true
    }], function (err, result) {
        //
        // Log the results.
        //

        gutil.log( gutil.colors.cyan('\n' + JSON.stringify(result, null, '\t')));

        let elExists = _.find(db.get(taxonomy).value(),function(t) {
            return t.title === result.title;
        });

        if (elExists) {
            let categories = db.get(taxonomy);

            let cat = categories.find(result).value();

            categories.remove(cat).write();

            console.log('ÉLÉMENT SUPPRIMÉ DANS ' + taxonomy + ': \n' + JSON.stringify(result, null, '\t'));
        } else {
            gutil.log( gutil.colors.red('Cette categorie n\'existe pas !'));
        }


    });
}

gulp.task('delete-category', function() {
    return deleteCategory();
});

module.exports = deleteCategory;