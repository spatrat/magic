const gulp = require('gulp');
const responsive = require('gulp-responsive');
const del = require('del');

gulp.task('thumbnails', function () {
    del('dist/img/posts/thumbnails');
    return gulp.src('src/img/posts/thumbnails/*.{png,jpg,jpeg}')
        .pipe(responsive({
                '*.jpeg': [
                {
                    width: 1920,
                    height: 560,
                    rename: {
                        suffix: '@large'
                    }
                },
                {
                    width: 1280,
                    height: 720,
                    rename: {
                        suffix: '@1280'
                    }
                },
                {
                    width: 768,
                    height: 420,
                    rename: {
                        suffix: '@768'
                    }
                },
                {
                    width: 512,
                    height: 288,
                    rename: {
                        suffix: '@512'
                    }
                },
                {
                    width: 128,
                    height: 128,
                    rename: {
                        suffix: '@128'
                    }
                }
            ]
        },
        {
            withoutEnlargement: true,
            skipOnEnlargement: false, // that option copy original file with/without renaming
            errorOnEnlargement: false
        }
        ))
        .pipe(gulp.dest('dist/img/posts/thumbnails'));
});