const gulp = require('gulp');
const frontMatter = require('gulp-front-matter');
const wrap = require('gulp-wrap');
const tap = require('gulp-tap');
const nunjucks = require('nunjucks');
const gutil = require('gulp-util');
const hljs = require('highlight.js');
const markdown = require('markdown-it');
const del = require('del');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const prompt = require('prompt');
const moment = require('moment');
const striptags = require('striptags');
const through = require('through2');
const gulpSequence = require('gulp-sequence');
const shortid = require('shortid');
const gfile = require('gulp-file');
const mkdirp = require('mkdirp');
const jsonConcat = require('gulp-json-concat');
const getFilepath = require('./lib/utils/getFilePath');
const exitWithMsg = require('./lib/utils/exitWithMessage');

// Low DB
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

// Set some defaults
// TODO : CHECK IF DB EXISTS, OTHERWISE CREATE IT

function initDb() {
    db.defaults({
        posts: [],
        taxonomies: {
            categories: [],
            tags: []
        }
    })
        .write();
}

function dbPushPost(post) {
    db.get('posts')
        .push(post)
        .write()
}

function dbGetPost(post_id) {
    console.log(db.get('posts')
        .find({ id: post_id })
        .value());
}


gulp.task('init-db', function() {
    return initDb();
});

gulp.task('db-push-post', function() {
   return dbPushPost({id: 1, title: 'lowdb is awesome'});
});

gulp.task('db-get-post', function() {
    return dbGetPost(1);
});



// Data
const siteData = require('../src/data/site-data.json');
const postsData = require('../db.json');
const globalData = Object.assign({}, siteData, postsData);

// Configure Nunjucks to not escape HTML
const nunEnv = nunjucks.configure({autoescape: false});


const md = new markdown({
    html: true,
    highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
            try {
                return hljs.highlight(lang, str).value;
            } catch (__) {}
        }

        return '';
    }
});

function createNewPost(i) {
    const name = process.argv[i+1];
    if ( ! name ) exitWithMsg('No file name entered');

    const filename = (name.indexOf(".md") > -1)
        ? name
        : `${name}.md`;
    const filenamed = (filename.indexOf(" ") > -1)
        ? filename.replace(/ /g, '-')
        : filename;
    const filepath = path.join('.', 'src/posts/drafts', filenamed);

    const title = (name.indexOf("-") > -1)
        ? name.replace(/-/g, ' ').replace('.md', '')
        : name.replace('.md', '');
    const titled = title.replace(/\b\w+/g, txt => {
        return txt.charAt(0).toUpperCase()
            + txt.substr(1).toLowerCase();
    });

    console.log('\n\n');
    // Start the prompt
    prompt.start();
    // Get two properties from the user: username and password
    prompt.get([{
        name: 'category',
        description: gutil.colors.cyan('Post category ?'),
        type: 'string',
        required: false
    }, {
        name: 'tags',
        description: gutil.colors.cyan('Post tags ?'),
        type: 'string',
        required: false
    },{
        name: 'thumbnail',
        description: gutil.colors.cyan('Post thumbnail (with extension) ?'),
        message: 'Please enter the image extension (.jpeg, .png etc.)',
        type: 'string',
        required: false
    }], function (err, result) {
        //
        // Log the results.
        //
        gutil.log( gutil.colors.cyan('Here are the new post informations:\n'));
        // Set moment.js locale
        moment.locale('fr-ca');

        // Here create the file base frontmatter
        const content = `---\ntitle: ${titled}\ncategory: ${result.category}\ntags: ${result.tags}\ndate: ${moment().format('llll')} 
thumbnail: ${result.thumbnail} \nfeatured: false\n---\n`;

        return fs.writeFileSync(filepath, content);

    });
}

const postsTask = function() {

    const i = process.argv.indexOf("--file");
    if ( i === -1 ) del('./dist/articles/*.html');

    db.set('posts', [])
        .write();
    return gulp.src(getFilepath(i, '.md', './src/posts/drafts'))
        .pipe(frontMatter())
        .pipe(tap( file => modifyContents(file)))
        .pipe(wrap(
            data => fs.readFileSync(
                './src/html/partials/post.nunjucks'
            ).toString(),
            globalData, /* HERE you can pass your site and other data to Nunjucks engine :) */
            {engine: 'nunjucks'}
        ))
        .pipe(gulp.dest('./dist'));
};


function modifyContents(file) {
    const contents = md.render(file.contents.toString());
    const lines = contents.split(/\n/);
    const newContents = assemble(lines);
    const fm = file.frontMatter;

    file.contents = new Buffer(newContents);
    file.path = gutil.replaceExtension(file.path, '.html');

    //fm.shortPath = gutil.replaceExtension(path.basename(file.path), '');

    if (typeof fm.tags !== 'undefined' && fm.tags !== null) {
        fm.tags = fm.tags.split(',');
    }

    if (typeof fm.thumbnail !== 'undefined' && fm.thumbnail !== null) {
        fm.thumbnail = fm.thumbnail.substring(0, fm.thumbnail.lastIndexOf('.'));
    }
    fm.exerpt = striptags(newContents.substring(0, 100)).replace(/(?:\r\n|\r|\n)/g, ' ') + ' (...)';
    fm.path = gutil.replaceExtension(path.basename(file.path), '');
    fm.post_id = shortid.generate();

    db.get('posts')
        .push(fm)
        .write();

    addElementToTaxonomy('taxonomies.categories', {title: fm.category});
    addElementToTaxonomy('taxonomies.tags', fm.tags);
    function addElementToTaxonomy(taxonomy, element) {
        if (typeof element !== 'undefined' && element !== null) {

            // Si l'élément à ajouter est un tableau
            if (Array.isArray(element)) {

                element.map(function(subEl) {
                    return addElement(taxonomy, subEl);
                });
                // Sinon
            } else {
                addElement(taxonomy, element);
            }

            // Ajout d'un élément
            // @params : taxonomy: string, element: string
            function addElement(taxonomy, element) {
                let elExists = db.get(taxonomy)
                    .value().indexOf(element) !== -1;

                if (typeof element === 'object') {
                    elExists = _.find(db.get(taxonomy).value(),function(t) {
                        return t.title === element.title;
                    });
                }

                if (!elExists) {
                    db.get(taxonomy)
                        .push(element)
                        .write();
                    console.log('NOUVEL ELEMENT AJOUTÉ DANS ' + taxonomy + ': ' + element );
                }
            }
        }
    }

    return file;

    function assemble(lines) {
        lines.forEach( (line,index) => {
            if ( line.indexOf("hljs") === -1 ) {
                lines[index] = regReplace(line);
            }
        });

        return lines.join('\n');
    }

    function regReplace(line) {
        return line.replace(/#39/g, 'rsquo')
            .replace(/'/g, '&rsquo;')
            .replace(/&quot;(.*?)&quot;/g, '&ldquo;$1&rdquo;')
            .replace(
                /(HTML|CSS|PHP|CLI|RSS|URL|OOP|TIOBE)/g,
                '<span class="smallcaps">$1</span>'
            );
    }
}


gulp.task('create-posts-markup', function() {
   return postsTask();
});

gulp.task('create-posts-json', function() {
    createPostJson();
});


// Posts Task
gulp.task('posts', () => {
    del('./src/data/tmp/**').then(function() {
        return gulpSequence(['create-posts-markup'], function() {
            gutil.log(gutil.colors.cyan('Posts markup is created , Posts data json file is created too'));

        });
    });

});


// Create new post task
gulp.task('new', () => {
    const i = process.argv.indexOf("--post");
    if ( i > -1 ) {
        createNewPost(i);

    } else {
        exitWithMsg("Usage: gulp new --post file-name");
    }
});

module.exports = postsTask;