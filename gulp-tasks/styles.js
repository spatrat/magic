const gulp = require('gulp');
const sass = require('gulp-sass');

const stylesTask = function() {
    gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'));
};

gulp.task('sass', function() {
    stylesTask();
});

module.exports = stylesTask;