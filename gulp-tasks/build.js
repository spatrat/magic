const gulp =require('gulp');
const gutil = require('gulp-util');
const gulpSequence = require('gulp-sequence');
const mkdirp = require('mkdirp');

// Environment setting
const env = 'dev';

gulp.task('init', function() {
    return mkdirp('./dist/data', function (err) {
        if (err) console.error(err)
        else console.log('Data folder created !');
    });
});

gulp.task('build', () => {
    gulpSequence('init','thumbnails', 'posts', 'html', 'sass', 'js', 'export-data', function() {
        gutil.log( gutil.colors.magenta('Build for ' + env + ' environment IS DONE.'))
    });
});