module.exports = function exitWithMsg(message) {
    process.stdout.write(`[        ] Error: ${message}\n`);
    process.exit(1);
};