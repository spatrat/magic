const path = require('path');
const fs = require('fs');
const exitWithMsg = require('./exitWithMessage');

module.exports = function getFilepath(i, ext, dir) {
    return ( i > -1 )
        ? singleFile()
        : path.join('.', dir, '**', `*${ext}`);

    function singleFile() {
        const file = process.argv[i+1];
        if ( ! file ) exitWithMsg('No filename entered');

        const bareFile = file.replace(/(.html|.md)/, '');
        const filepath =
            path.join('.', dir, `${bareFile}${ext}`);
        fs.access(filepath, fs.F_OK, err => {
            if ( err ) exitWithMsg(`${filepath} not found`);
        });
        return filepath;
    }
}