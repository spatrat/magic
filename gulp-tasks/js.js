const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('../webpack.config.js');
const gutil = require('gulp-util');


const jsTask = function() {
    console.log('Starting javascript task!');
    return gulp.src('src/js/*.js')
        .pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest('dist/js'));
};


gulp.task('js', function() {
    jsTask();
    return gutil.log( gutil.colors.magenta('Javascript task is done.'));
});

module.exports = jsTask;
